import express from "express";
import morgan from "morgan";
import cors from "cors";

const routes = require("./app.routes");

const app = express();
require('./database/databaseConnection'); // Database connection 

// SETTINGS
app.set('port', process.env.PORT || 4000);

// MIDDELWARES
app.use(express.json());
app.use(cors());
app.use(morgan('dev'))

// ROUTES
app.use('/api', routes);

// SERVER RUN
app.listen( app.get('port'), () => {
    console.log(`Server listening in port:: ${app.get('port')} :)`);
});
