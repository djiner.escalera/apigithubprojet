import express, {Request, Response} from "express";

const router = express.Router();

import HTTPRequest from "./classes/HTTPRequest";

router.get( '/users', (req: Request, res: Response) => {
    const http_service = new HTTPRequest();
    http_service.getUsers(req, res);
});

router.get( '/users/:id', (req: Request, res: Response) => {
    const http_service = new HTTPRequest();
    http_service.getRepositories(req, res);
});

module.exports = router;
