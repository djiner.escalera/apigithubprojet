import * as React from 'react';
import RepositoryCard from "./RepositoryCard";
// import { RouteComponentProps } from 'react-router';

interface IProps { 
    match: { 
        params: { 
            id: string; 
        }; 
    }; 
}

interface IState {
    username: string;
    repositoriesList: string[];
}

class Repositories extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = {
            username: props.match.params.id ,
            repositoriesList: []
        };

        this.getRepositoriesFromServer = this.getRepositoriesFromServer.bind(this);
    }

    componentDidMount() {
        this.getRepositoriesFromServer();
    }

    async getRepositoriesFromServer() {

        const URL = "http://localhost:4000/api/users/"+ this.state.username ;
        
        try {
            const response = await fetch(URL);
            console.log("RES", response);

            if (response.ok) {
                const data = await response.json();
                this.setState({
                    repositoriesList: data
                });
                console.log("REPOS:::",this.state.repositoriesList);
            }          

        } catch (error) {
            console.log(error);
        }
    }

    render() {

        const username = <span> {this.state.username} </span>;
        const title =  <h1 className="text-center text-uppercase"> {username} repositories  </h1>

        const repositoiresCards = this.state.repositoriesList.map( (item: any, key: number) => 
            <div key={key} className="col-lg-3 col-sm-6">
                <RepositoryCard item={item}/> 
            </div>
        );
        
        return (
            
            <div className="container">
                {/* <div className="row"> */}
                    { title }
                {/* </div> */}
                <div className="row">
                    { repositoiresCards }
                </div>
            </div>
        );
    }
}

export default Repositories;