import React from 'react';
import { Link } from "react-router-dom";

class Navbar extends React.Component {
    render() {
        return(
            <nav className="navbar navbar-expand-lg navbar navbar-dark bg-dark">
                <div className="container">
                <Link to="/" className="navbar-brand"> Home </Link>
                
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                        {/* <a className="navbar-brand" href="/">GitHub Users</a> */}
                        {/* <Link to="/" className="navbar-brand"> Home </Link> */}
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li className="nav-item active">
                                {/* <Link to="/user" className="nav-link"> User </Link> */}
                            </li>
                        </ul>
                    </div>
                </div>
                
            </nav>
        );
    }
}

export default Navbar;