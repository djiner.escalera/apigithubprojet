// import React, { Component } from 'react';
import * as React from 'react';
import gitlogodefault from '../../assets/img/github.png'
import { Link } from "react-router-dom";

interface IProps {
    item: any;
}

interface IState {
    username?: any;
    githubUrl?: string;
    avatar_url?: string;
}

class UserCard extends React.Component<IProps, IState> {

    state: IState;

    constructor(props: IProps) {
        super(props);

        this.state = {
            username: props.item.login,
            githubUrl: props.item.html_url,
            avatar_url: props.item.avatar_url
        };
        
        this.handlerAvatar = this.handlerAvatar.bind(this);
    }

    handlerAvatar() {
        let avatar;

        if (this.state.avatar_url !== "") {
            avatar = <img src={ this.state.avatar_url } className="card-img-top" alt=":("/>;
        } else {
            avatar = <img src={ gitlogodefault } className="card-img-top" alt=":("/>;
        }
        return avatar;
    }

    render() {
        const avatar = this.handlerAvatar();

        const card = (
            <div className="card mb-3">
                { avatar }
                <div className="card-body">
                    <h4 className="card-title text-center">{this.state.username.toUpperCase()}</h4>
                    <a href={this.state.githubUrl} className="btn-link" target="_blank" rel="noopener noreferrer">Go to github</a>
                    <p className="card-text">Lorem ipsum...</p>
                    <Link to={this.state.username} className="btn btn-primary btn-block"> Repositories </Link>
                </div>
            </div>
        );
        return card;
    }
}

export default UserCard;