import mongoose from "mongoose";
import {Schema} from "mongoose";

const Users = new Schema({
    request: {type: String, required: true, unique: true, dropDups: true},
    data: { type: Object }
});

module.exports = mongoose.model('Users', Users);