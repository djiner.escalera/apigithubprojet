import * as React from 'react';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import './App.css';
import Navbar from "./components/Shared/Navbar";
import Users from "./components/Users/Users";
import Repositories from "./components/Users/Repositories";

const App: React.FC = () => {
  return (
      <Router>
        <Navbar/>

        <Switch>
          <Route exact path="/" component={ Users } />
          <Route path="/:id" component={ Repositories } />
        </Switch>
      </Router>
  );
}

export default App;
