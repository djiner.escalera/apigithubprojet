# APP SHOW USERS FROM GITHUB

This project use Nodejs & Reactjs

## Getting Started

### Prerequisites

You need: 

```
MongoDB 
```

### Installing

**SERVER**

* STEP 1 - Open a terminal and go to project
* STEP 2 - Go to "server" folder
* STEP 3 - Run the commnand "$npm install"
* STEP 4 - Run the command "$npm run dev"

```
Done
```

**FRONTEND**

* STEP 1 - Open a new terminal and go to project
* STEP 2 - Go to "frontend" folder
* STEP 3 - Run the commnand "$npm install"
* STEP 4 - Run the command "$npm start"



```
Done
```

