import * as React from 'react';

interface IProps {
    item: any;
}

interface IState {
    name?: any;
    description?: string;
    githubUrl?: string;
    forks?: number;
    issues?: number;
}

class RepositoryCard extends React.Component<IProps, IState> {

    state: IState;

    constructor(props: IProps) {
        super(props);

        this.state = {
            name: props.item.name,
            description: props.item.description,
            githubUrl: props.item.html_url,
            forks: props.item.forks_count,
            issues: props.item.open_issues_count
        }
    }

    render() {
        return (
            <div className="card text-white bg-dark mb-3">
                <div className="card-header">
                    <h4 className="card-title">{ this.state.name.toUpperCase() }</h4>
                </div>
                <div className="card-body">
                    <a href={ this.state.githubUrl } className="btn-link" role="button" target="_blank" rel="noopener noreferrer">Go to github</a>
                    <p className="card-text"> { this.state.description } </p>
                </div>
                <div className="card-footer">
                    <span>Forks: { this.state.forks } </span> 
                    <span>Issues: { this.state.issues }</span>
                </div>
                <div></div>
            </div>
        );
    }
}

export default RepositoryCard;