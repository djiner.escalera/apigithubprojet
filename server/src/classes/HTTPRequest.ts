import express, {Request, Response} from "express";
import fetch from 'node-fetch';

import { UserService } from "../services/user.service";
import { RepositoryService } from "../services/repository.service";

class HTTPRequest {
    API_URL: string;

    constructor() {
        this.API_URL = "https://api.github.com/users";
    }

    async getUsers( req: Request, res: Response ) {

        const userList = await UserService.existInDatabase( req.originalUrl );
        console.log("URL----->", req.originalUrl);
        
        if ( userList ) {
            console.log("[DATA FROM]::: Database");
            res.send(userList.data);
        } else {
            console.log("[DATA FROM]::: Api Github");

            await this.getUsersFromGithub(res);
        } 
    }

    async getRepositories( req: Request, res: Response ) {
        const { id } = req.params;
        console.log("URL----->", req.originalUrl);

        const repositoryList = await RepositoryService.existInDatabase(req.originalUrl);

        if (repositoryList) {
            console.log("[DATA FROM]::: Database");
            res.send(repositoryList.data);
        } else {
            console.log("[DATA FROM]::: Api Github");

            await this.getRepositoriesFromGithub(id, res);
        }
    }

    private async getRepositoriesFromGithub(id: string, res: Response) {
        try {
            const response = await fetch(this.API_URL + `/${id}` + "/repos?page=1&per_page=100");
            
            if (response.ok) {
                const data = await response.json();
                RepositoryService.saveRepositories(id, data);
                res.json(data);
            }
            else {
                res.status(500).send('Something broke!');
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    private async getUsersFromGithub(res: Response) {
        try {
            const response = await fetch(this.API_URL + "?page=1&per_page=100");
            
            if (response.ok) {
                const data = await response.json();
                UserService.saveUsers(data);
                res.json(data);
            } else {
                res.status(500).send('Something broke!');
            }
        } catch (error) {
            console.log(error);
        }
    }
}

export default HTTPRequest;