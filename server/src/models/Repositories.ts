import mongoose from "mongoose";
import {Schema} from "mongoose";

const Repositories = new Schema({
    request: {type: String, unique : true, required : true, dropDups: true},
    data: { type: Object }
});

module.exports = mongoose.model('Repositories', Repositories);