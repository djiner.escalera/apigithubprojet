import * as React from 'react';
import UserCard from './UserCard'

interface IProps {
    status: boolean;
}

interface IState {
    usersList: string[];
}

class Users extends React.Component<IProps, IState> {

    state: IState = { 
        usersList: [] 
    };
    
    constructor(props: IProps) {
        super(props);
        
        // Methods
        this.getUsersFromServer = this.getUsersFromServer.bind(this);
    }

    componentDidMount() {
        this.getUsersFromServer();
    }

    async getUsersFromServer() {
        const URL = "http://localhost:4000/api/users"; 

        try {
            const response = await fetch(URL);
            console.log("RES", response);

            if (response.ok) {
                const data = await response.json();
                console.log(data);
                this.setState({
                    usersList: data
                });
            }          
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const tittle = ( <h1 className="text-center text-uppercase"> Github Users </h1> );

        const usersCards = this.state.usersList.map( (item, key) => 
            <div key={key} className="col-lg-3 col-sm-6">
                <UserCard  item={item} />
            </div>
        );
       
        return (
            <div className="container">
                { tittle }
                <div className="row">
                    { usersCards }
                </div>
            </div>
        );
    }
}

export default Users;