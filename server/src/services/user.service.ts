const Users = require("../models/Users");

export class UserService {

    static async saveUsers(json: object) {
        const newUserData = new Users({
            request: "http://localhost:4000/api/users",
            data: json
        });
    
        await newUserData.save();
    }

    static async existInDatabase(url: string) {
        const request = "http://localhost:4000" + url;
        
        try {
            const data = await Users.findOne({request: request});    
            return data;
        } catch (error) {
            console.log(error);
        }
    }
}
