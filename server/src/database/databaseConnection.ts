import mongoose from "mongoose";

const url: string = 'mongodb://localhost/github_database';

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.connect(url, { useNewUrlParser: true })
    .then(db => {
        console.log('>>> Database connected xD');
    }).catch(error => {
        console.log(error);
    });

export default mongoose;