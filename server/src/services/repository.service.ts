const Repositories = require("../models/Repositories");

export class RepositoryService {

    static async saveRepositories(user: string, json: object) {

        const newRepositoriesData = new Repositories({
            request: `http://localhost:4000/api/users/${user}`,
            data: json
        });
    
        await newRepositoriesData.save();
    }

    static async existInDatabase(url: string) {
        const request = "http://localhost:4000" + url;
        
        try {
            const data = await Repositories.findOne({request: request});    
            return data;
        } catch (error) {
            console.log(error);
        }
    }
}
